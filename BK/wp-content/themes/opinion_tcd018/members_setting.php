<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
// 会員限定 --------------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////////////////////

//ツールバー設定(購読者：非表示)
if(current_user_can('subscriber')){
	add_action( 'after_setup_theme', 'subscriber_hide_toolbar' );
}
function subscriber_hide_toolbar() {
	show_admin_bar( false );
}

//管理画面アクセス設定(購入者：禁止 アクセス時TOPへリダイレクト)
add_action( 'auth_redirect', 'subscriber_go_to_home' );

function subscriber_go_to_home( $user_id ) {

	$user = get_userdata( $user_id );

	if ( !$user->has_cap( 'edit_posts' ) ) {
		wp_redirect( get_home_url() );
		exit();

	}
}

//メニューバー設定(「ユーザー」の項目に会員リスト追加)
add_action ( 'admin_menu', 'artist_add_pages' );
function artist_add_pages () {
	add_users_page('会員リスト', '会員リスト', 8, 'users.php?role=subscriber', '');
}

// サイドバーのログインウィジェットにリンク追加
function my_sidebar_status( $string )
{   
	$current_user = wp_get_current_user();
	$args = "<p>こんにちは $current_user->display_name さん</p>"; 
    $string = "<ul><li><a href=\"/conekuto/mypage/\">マイページ</a></li><li><a href=\"./?a=logout\">ログアウト</a></li></ul>";
    return $args . $string;
}
add_filter( 'wpmem_sidebar_status', 'my_sidebar_status' );
