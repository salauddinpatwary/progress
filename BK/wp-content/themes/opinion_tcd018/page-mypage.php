<?php
/*
Template Name:user_mypage_template
*/
?>
<?php get_header(); $options = get_desing_plus_option(); ?>

<!--ヘッダーレイアウト設定ここから-->
<div id="main_col">

 <ul id="bread_crumb" class="clearfix">
  <li class="home"><a href="<?php echo esc_url(home_url('/')); ?>"><span><?php _e('Home', 'tcd-w'); ?></span></a></li>
  <li class="last"><?php the_title(); ?></li>
 </ul>

 <div id="left_col">
<!--ヘッダーレイアウト設定ここまで-->

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <?php if (is_user_logged_in()) : //ログイン時に表示?>
  <p>こんにちは、<?php echo do_shortcode('[wpmem_field display_name]'); ?>さん。</p>
  <a href="./?a=logout">ログアウト</a>
  
  <?php else : //未ログイン時?>
  <p>会員ユーザーは下記よりログインして下さい。</p>
  <?php endif; ?>
  
  <div class="post clearfix">

   <?php the_content(__('Read more', 'tcd-w')); ?>
   <?php custom_wp_link_pages(); ?>

  </div><!-- END .post -->


<!--フッターレイアウト設定ここから-->
  <?php endwhile; endif; ?>

 </div><!-- END #left_col -->

</div><!-- END #main_col -->

<?php get_footer(); ?>
<!--フッターレイアウト設定ここまで-->