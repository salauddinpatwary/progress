<?php 
/**
* Footer template used by Neuro.
*
* Authors: Tyler Cunningham, Trent Lapinski
* Copyright: © 2012
* {@link http://cyberchimps.com/ CyberChimps LLC}
*
* Released under the terms of the GNU General Public License.
* You should have received a copy of the GNU General Public License,
* along with this software. In the main directory, see: /licensing/
* If not, see: {@link http://www.gnu.org/licenses/}.
*
* @package Neuro.
* @since 2.0
*/

	global $options, $ne_themeslug // call globals

?>

<!-- For sticky footer -->
<div class="push"></div>  
</div>	<!-- End of wrapper -->

<?php if ($options->get($ne_themeslug.'_disable_footer') != "0"):?>	

</div><!--end container wrap-->

<div class="footer"> <!-- Footer class for sticky footer -->
    <div id="footer" class="container">
     		<div class="row" id="footer_container">
    			<div id="footer_wrap">	
	<!-- Begin @response footer hook content-->
		<?php response_footer(); ?>
	<!-- End @response footer hook content-->
				</div>
			<div class="row" >
				<div id="credit" class="twelve columns">
					<a href="http://cyberchimps.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/achimps.png" alt="credit" /></a>
				
				</div>
			</div>
	<?php endif;?>
	

			</div><!--end footer_wrap-->
	</div><!--end footer-->

<?php if ($options->get($ne_themeslug.'_disable_afterfooter') != "0"):?>

	<div id="afterfooter" class="container">
		<div class="row" id="afterfooterwrap">	
		<!-- Begin @response afterfooter hook content-->
			<?php response_secondary_footer(); ?>
		<!-- End @response afterfooter hook content-->
				
		</div> <!--end afterfooter-->	
	</div> 	
	<?php endif;?>	
	
</div>  <!--End of footer class for sticky footer -->	

<?php wp_footer(); ?>

<div style="position: absolute; left: -16000px;">
<a href="http://www.getpicksneaker.com/" title="Retro jordans for sale">Retro jordans for sale</a>, 
<a href="http://www.getpicksneaker.com/product.asp?txtitle=nike+foamposite">Cheap foamposites</a>,
<a href="http://www.usliketoo.com/x-sale/?Air-Jordan-Retro-5_1222_1.html">jordan retro 5</a>,
<a href="http://www.getpickshoe.com/" title="cheap jordans for sale">cheap jordans for sale</a>,
<a href="http://www.getpickshoe.com/find-foamposites.htm" title="foamposites For Sale">foamposites For Sale</a>,
<a href="http://www.getpicksneaker.com/product.asp?txtitle=jordan+retro+12">jordan retro 12</a>,
<a href="http://www.getpicksneaker.com/product.asp?txtitle=jordan+retro+11">jordan retro 11</a>,
<a href="https://www.gotoruns.com/" title="Cheap jordans for sale">Cheap jordans for sale</a>,
<a href="http://www.usliketoo.com/" title="jordans for cheap">jordans for cheap</a>,
<a href="http://www.usliketoo.com/product.asp?txtitle=jordan+retro+11+legend+blue">jordan retro 11 legend blue</a>,
<a href="http://www.usliketoo.com/product.asp?txtitle=retro+12+jordans">retro 12 jordans</a>,
<a href="https://www.justdogo.com/" title="cheap soccer jerseys">cheap soccer jerseys</a>,
<a href="http://www.wedownjackets.com/" title="Canada Goose sale">Canada Goose sale</a>,
<a href="http://www.aboutmyfoot.com/" title="cheap jordans">cheap jordans</a>,
<a href="http://www.getpickshoe.com/find-jordan-12.htm" title="cheap jordan 12">cheap jordan 12</a>
</div></body>

</html>
