<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
<meta name="description" content="<?php bloginfo('description'); ?>" />

<link href="/css/layout_cmn.css" rel="stylesheet" type="text/css" />
<style type="text/css" media="screen">
@import url( <?php bloginfo('stylesheet_url'); ?> );
</style>

<link rel="stylesheet" type="text/css" href="layout_cmn.css" />

<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php if (is_home()): ?><?php wp_get_archives('type=monthly&format=link'); ?><?php endif; ?>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="header">
<h1>土地活用 資産運用セミナーなら資産パートナープランナーズ</h1>
<div id="headlogo">
<a href="/"><img src="/img_cmn/header/img_logo.jpg" width="292" height="49" alt="img_logo.jpg" /></a>
</div>
<!--end headlogo-->
<div id="headtel">
<img src="/img_cmn/header/txt_tel.jpg" width="382" height="49" alt="txt_tel.jpg" />
</div>
<!--end headtel-->
<div id="headcontact">
<a href="/contact/"><img src="/img_cmn/header/btn_contact.jpg" width="200" height="51" alt="btn_contact.jpg" class="btn" /></a>
</div>
<!--end contact-->
</div>
<!--end header-->
<div id="bg_glovalnav">
<div id="glovalnav">
<ul class="cf clearfix">
<li><a href="/"><img src="/img_cmn/gn/gn_home.jpg" width="130" height="37" alt="gn_home.jpg" class="btn" /></a></li>
<li><a href="/specialist/"><img src="/img_cmn/gn/gn_specialist.jpg" width="131" height="37" alt="gn_specialist.jpg" class="btn" /></a></li>
<li><a href="/shisan/"><img src="/img_cmn/gn/gn_shisan.jpg" width="131" height="37" alt="gn_shisan.jpg" class="btn" /></a></li>
<li><a href="/media/"><img src="/img_cmn/gn/gn_media.jpg" width="131" height="37" alt="gn_media.jpg" class="btn" /></a></li>
<li><a href="/faq/"><img src="/img_cmn/gn/gn_faq.jpg" width="131" height="37" alt="gn_faq.jpg" class="btn" /></a></li>
<li><a href="/company/"><img src="/img_cmn/gn/gn_comany.jpg" width="131" height="37" alt="gn_comany.jpg" class="btn" /></a></li>
<li><a href="/contact/"><img src="/img_cmn/gn/gn_contact.jpg" width="128" height="37" alt="gn_contact.jpg" class="btn" /></a></li>
</ul>
</div>
<!--end glovalnav-->
</div>
<!--end bg_glovalnav-->
<div id="main_img_sp_blog">
<div id="contents" class="cf  clearfix">