<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>専門家プロフィール</title>
<link href="/css/layout_cmn.css" rel="stylesheet" type="text/css" />
</head>

<body>

<!--専門家リスト-->

<div id="specialist_list" class="clearfix">
<!--専門家-->
<div class="sp_men clearfix">
<div class="image"><a href="<?php bloginfo('url'); ?>/specialist01/"><img src="<?php bloginfo('template_directory'); ?>/img/img_photo1.jpg" width="120" height="140" alt="西田芳明 氏" /></a></div>
<div class="text">
<h4>建築コンサルタント</h4>
<h5>西田芳明 氏</h5>
<p class="font10">進和建設工業株式会社/代表取締役</p>
<p><a href="http://www.e-shinwa.net/" target="_blank">http://www.e-shinwa.net/</a></p>
<ul>
<li><a href="/specialist_blog/specialist01/73.html">専門家プロフィール</a></li>
<?php
$posts = get_posts('numberposts=3&category=2');
global $post;
?>
<?php
if($posts): foreach($posts as $post): setup_postdata($post); ?>
<li>
<a href="<?php the_permalink(); ?>">
<?php 
echo mb_strimwidth($post->post_title, 0, 20, "..."); 
?></a>
</li>
<?php endforeach; endif;
?>
</ul>
</div>
</div>
<!--/専門家-->

<!--専門家-->
<div class="sp_men clearfix">
<div class="image"><a href="<?php bloginfo('url'); ?>/specialist02/"><img src="<?php bloginfo('template_directory'); ?>/img/img_photo2.jpg" width="120" height="140" alt="今仲清 氏" /></a></div>
<div class="text">
<h4>税理士</h4>
<h5>今仲清 氏</h5>
<p class="font10">今仲税理士事務所／所長</p>
<p><a href="http://www.imanaka-kaikei.co.jp/pc/" target="_blank">http://www.imanaka-kaikei.co.jp/pc/</a></p>
<ul>
<li><a href="/specialist_blog/specialist02/12.html">専門家プロフィール</a></li>
<?php
$posts = get_posts('numberposts=3&category=4');
global $post;
?>
<?php
if($posts): foreach($posts as $post): setup_postdata($post); ?>
<li>
<a href="<?php the_permalink(); ?>">
<?php 
echo mb_strimwidth($post->post_title, 0, 20, "..."); 
?></a>
</li>
<?php endforeach; endif;
?>
</ul>
</div>
</div>
<!--/専門家-->

<!--専門家-->
<div class="sp_men clearfix">
<div class="image"><a href="<?php bloginfo('url'); ?>/specialist03/"><img src="<?php bloginfo('template_directory'); ?>/img/img_photo3.jpg" width="120" height="140" alt="谷敦 氏" /></a></div>
<div class="text">
<h4>ファイナンシャルプランナー</h4>
<h5>谷敦 氏</h5>
<p class="font10">株式会社 JUST FOR YOU／代表取締役</p>
<p><a href="http://www.j-f-y.net/" target="_blank">http://www.j-f-y.net/</a></p>
<ul>
<li><a href="/specialist_blog/specialist03/67.html">専門家プロフィール</a></li>
<?php
$posts = get_posts('numberposts=3&category=6');
global $post;
?>
<?php
if($posts): foreach($posts as $post): setup_postdata($post); ?>
<li>
<a href="<?php the_permalink(); ?>">
<?php 
echo mb_strimwidth($post->post_title, 0, 20, "..."); 
?></a>
</li>
<?php endforeach; endif;
?>
</ul>
</div>
</div>
<!--/専門家-->

<!--専門家-->
<div class="sp_men clearfix">
<div class="image"><a href="<?php bloginfo('url'); ?>/specialist04/"><img src="<?php bloginfo('template_directory'); ?>/img/img_photo4.jpg" width="120" height="140" alt="和田清人 氏" /></a></div>
<div class="text">
<h4>土地家屋調査士</h4>
<h5>和田清人 氏</h5>
<p class="font10">和田清人測量登記事務所／土地家屋調査士・測量士</p>
<p><a href="http://www.e-souzoku.com/index.php" target="_blank">http://www.e-souzoku.com/index.php</a></p>
<ul>
<li><a href="/specialist_blog/specialist04/71.html">専門家プロフィール</a></li>
<?php
$posts = get_posts('numberposts=3&category=8');
global $post;
?>
<?php
if($posts): foreach($posts as $post): setup_postdata($post); ?>
<li>
<a href="<?php the_permalink(); ?>">
<?php 
echo mb_strimwidth($post->post_title, 0, 20, "..."); 
?></a>
</li>
<?php endforeach; endif;
?>
</ul>
</div>
</div>
<!--/専門家-->

<!--専門家-->
<div class="sp_men clearfix">
<div class="image"><a href="<?php bloginfo('url'); ?>/specialist05/"><img src="<?php bloginfo('template_directory'); ?>/img/img_photo5.jpg" width="120" height="140" alt="勝　猛一氏" /></a></div>
<div class="text">
<h4>相続税問題スペシャリスト</h4>
<h5>勝　猛一氏</h5>
<p class="font10">相続遺言サポートオフィス ゆずりは/司法書士</p>
<p><a href="http://www.souzokutokyo.com/" target="_blank">http://www.souzokutokyo.com/</a></p>
<ul>
<li><a href="/specialist_blog/specialist05/60.html">専門家プロフィール</a></li>
<?php
$posts = get_posts('numberposts=3&category=9');
global $post;
?>
<?php
if($posts): foreach($posts as $post): setup_postdata($post); ?>
<li>
<a href="<?php the_permalink(); ?>">
<?php 
echo mb_strimwidth($post->post_title, 0, 20, "..."); 
?></a>
</li>
<?php endforeach; endif;
?>
</ul>
</div>
</div>
<!--/専門家-->

<!--専門家-->
<div class="sp_men clearfix">
<div class="image"><a href="<?php bloginfo('url'); ?>/specialist06/"><img src="<?php bloginfo('template_directory'); ?>/img/img_photo6.jpg" width="120" height="140" alt="吉村　洋文氏" /></a></div>
<div class="text">
<h4>賃貸人の権利スペシャリスト</h4>
<h5>吉村　洋文氏</h5>
<p class="font10">イデア綜合法律事務所/弁護士</p>
<p><a href="http://www.idea-law.jp/" target="_blank">http://www.idea-law.jp/</a></p>
<ul>

<?php
$posts = get_posts('numberposts=3&category=10');
global $post;
?>
<?php
if($posts): foreach($posts as $post): setup_postdata($post); ?>
<li>
<a href="<?php the_permalink(); ?>">
<?php 
echo mb_strimwidth($post->post_title, 0, 20, "..."); 
?></a>
</li>
<?php endforeach; endif;
?>
</ul>
</div>
</div>
<!--/専門家-->

<!--専門家-->
<div class="sp_men clearfix">
<div class="image"><a href="<?php bloginfo('url'); ?>/specialist07/"><img src="<?php bloginfo('template_directory'); ?>/img/img_photo7.jpg" width="120" height="140" alt="太田垣　章子氏" /></a></div>
<div class="text">
<h4>司法書士</h4>
<h5>太田垣　章子氏</h5>
<p class="font10">章（あや）司法書士法人/代表司法書士</p>
<p><a href="http://www.ohtagaki.jp/" target="_blank">http://www.ohtagaki.jp/</a></p>
<ul>
<li><a href="/specialist_blog/specialist07/65.html">専門家プロフィール</a></li>
<?php
$posts = get_posts('numberposts=3&category=11');
global $post;
?>
<?php
if($posts): foreach($posts as $post): setup_postdata($post); ?>
<li>
<a href="<?php the_permalink(); ?>">
<?php 
echo mb_strimwidth($post->post_title, 0, 20, "..."); 
?></a>
</li>
<?php endforeach; endif;
?>
</ul>
</div>
</div>
<!--/専門家-->

<!--専門家-->
<div class="sp_men clearfix">
<div class="image"><a href="<?php bloginfo('url'); ?>/specialist08/"><img src="<?php bloginfo('template_directory'); ?>/img/img_photo8.jpg" width="120" height="140" alt="的場順子氏" /></a></div>
<div class="text">
<h4>司法書士</h4>
<h5>的場順子氏</h5>
<p class="font10">章（あや）司法書士法人/代表司法書士</p>
<p><a href="http://www.ohtagaki.jp/" target="_blank">http://www.ohtagaki.jp/</a></p>
<ul>
<li><a href="/specialist_blog/specialist08/69.html">専門家プロフィール</a></li>
<?php
$posts = get_posts('numberposts=3&category=12');
global $post;
?>
<?php
if($posts): foreach($posts as $post): setup_postdata($post); ?>
<li>
<a href="<?php the_permalink(); ?>">
<?php 
echo mb_strimwidth($post->post_title, 0, 20, "..."); 
?></a>
</li>
<?php endforeach; endif;
?>
</ul>
</div>
</div>
<!--/専門家-->

<!--専門家-->
<!--
<div class="sp_men clearfix">
<div class="image"><a href="<?php bloginfo('url'); ?>/specialist09/"><img src="<?php bloginfo('template_directory'); ?>/img/img_photo9.jpg" width="120" height="140" alt="岡崎謙二氏" /></a></div>
<div class="text">
<h4>ファイナンシャルプランナー</h4>
<h5>岡崎謙二 氏</h5>
<p class="font10">FPコンサルティング</p>
<p><a href="http://www.fp-con.co.jp/" target="_blank">http://www.fp-con.co.jp/</a></p>
<ul>
<?php
$posts = get_posts('numberposts=3&category=5');
global $post;
?>
<?php
if($posts): foreach($posts as $post): setup_postdata($post); ?>
<li>
<a href="<?php the_permalink(); ?>">
<?php 
echo mb_strimwidth($post->post_title, 0, 20, "..."); 
?></a>
</li>
<?php endforeach; endif;
?>
</ul>
</div>
</div>
-->
<!--/専門家-->

<!--専門家-->
<!--
<div class="sp_men clearfix">
<div class="image"><a href="<?php bloginfo('url'); ?>/specialist10/"><img src="<?php bloginfo('template_directory'); ?>/img/img_photo10.jpg" width="120" height="140" alt="林弘明 氏" /></a></div>
<div class="text">
<h4>不動産コーディネーター</h4>
<h5>林弘明 氏</h5>
<p class="font10">ハート財産パートナーズ</p>
<p><a href="http://www.hap.co.jp/" target="_blank">http://www.hap.co.jp/</a></p>
<ul>
<?php
$posts = get_posts('numberposts=3&category=3');
global $post;
?>
<?php
if($posts): foreach($posts as $post): setup_postdata($post); ?>
<li>
<a href="<?php the_permalink(); ?>">
<?php 
echo mb_strimwidth($post->post_title, 0, 20, "..."); 
?></a>
</li>
<?php endforeach; endif;
?>
</ul>
</div>
</div>
-->
<!--/専門家-->

</div>

<!--/専門家リスト-->



</body>
</html>