<?php // Do not delete these lines
    if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
        die ('Please do not load this page directly. Thanks!');
    if (!empty($post->post_password)) { // if there's a password
        if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) {  // and it doesn't match the cookie
            ?>
            <p class="nocomments">This post is password protected. Enter the password to view comments.<p>
            <?php return;
        }
    }
?>

<div id="comments">
<?php if (!('open' == $post->comment_status) && ('open' == $post->ping_status)) : // comment close ?> 
<p class="nocomments">Comments are closed.</p>
<?php endif; ?>


<?php if ('open' == $post->comment_status) : ?>
<p class="comments-head">コメントの投稿</p>

<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
<p>You must be <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php the_permalink(); ?>">logged in</a> to post a comment.</p>
<?php else : ?>

<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" name="commentform" id="commentform">
<div id="comments-open-data">
<?php if ( $user_ID ) : ?>
<p>Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. (<a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="Log out of this account">Logout</a>)</p>
<?php else : ?>
<p><input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="2" accesskey="n" />&nbsp;ニックネーム（必須）</p>
<p><input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="3" accesskey="m" />&nbsp;メールアドレス（非公開）</p>
<p><input type="text" name="url" id="url" value="<?php echo esc_attr($comment_author_url); ?>" size="22" tabindex="3" />&nbsp;ウェブサイト</p>
<?php endif; ?>
<p>コメント<br />
<textarea name="comment" id="comment" cols="40" rows="8" tabindex="5" accesskey="c"></textarea>
</p>
<p>
<input name="submit" type="submit" id="submit" tabindex="6" value="コメント" accesskey="s" />
<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
</p>

<?php do_action('comment_form', $post->ID); ?>
</div>
</form>


<p class="comments-head">コメント一覧&nbsp;&nbsp;<?php comments_number('0','1','%'); ?>件</p>
<div class="commentlist">
<?php foreach ($comments as $comment) : ?>

<? if ($comment->comment_type != "trackback" && $comment->comment_type != "pingback" && !ereg("<pingback />", $comment->comment_content) && !ereg("<trackback />", $comment->comment_content)) { ?>

<div id="comment-<?php comment_ID() ?>" class="comment">
<?php if ($comment->comment_approved == '0') : ?>
しばらく時間をおいて、再度お試しください。
<?php endif; ?>
<div class="comment-content"><?php comment_text() ?></div>
<p class="comment-footer"><?php comment_date('Y年m月j日') ?>&nbsp;&nbsp;<?php comment_author_link() ?><?php if ( $user_ID ) : ?><?php endif; ?></p>
</div>
<? } ?>

<?php endforeach; /* end for each comment */ ?>
</div>

<?php endif; // If registration required and not logged in ?>
<?php endif; // if you delete this the sky will fall on your head ?>

</div><!-- comments -->