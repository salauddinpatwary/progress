<?php get_header(); ?>

<?php get_sidebar(); ?>

<h2><img src="<?php bloginfo('template_url'); ?>/img/h2.jpg" width="620" height="76" alt="資産パートナープランナーズ通信" /></h2>
<div id="blogContents">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

<?php if(is_single()): ?>
<h3 class="storytitle"><?php the_title(); ?></h3>
<p class="meta"><?php the_time('Y.m.d'); ?>　<?php the_author(); ?></p>
<div class="storycontent">
<?php the_content(); ?>
</div><!--storycontent-->

<?php  else: ?>
<h3 class="storytitle"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
<p class="meta"><?php the_time('Y.m.d'); ?></p>
<div class="storycontent">
<?php the_excerpt(); ?>
</div><!--storycontent-->
<p class="rest "><a href="<?php the_permalink() ?>">&gt;&gt;続きを読む</a></p>
<?php endif; ?>
</div><!--post-->

<?php endwhile; else: ?>
<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

<?php posts_nav_link(' &#8212; ', __('&laquo; Newer Posts'), __('Older Posts &raquo;')); ?>

</div><!--blogContents-->

<div id="blogMenu">
<p><img src="<?php bloginfo('template_url'); ?>/img/recent.png" width="150" height="32" alt="最近の記事" /></p>
<ul>
<?php wp_list_categories('exclude=1&title_li='); ?> 
</ul>

</div><!--blogMenu-->

<div class="contentsbox mb30">
<a href="http://www.progress-pp.jp/contact/"><img src="/img/top/bnr_contact.jpg" width="615" height="97" alt="bnr_contact.jpg" class="btn" /></a>
</div>
<!--専門家リスト-->
<iframe width="615" height="1779" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://progress-pp.jp/specialist_blog/senmonka_profile"></iframe>
<!--/専門家リスト-->
<?php get_footer(); ?>