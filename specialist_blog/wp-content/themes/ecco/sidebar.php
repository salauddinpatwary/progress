<div id="side">
<div id="sidenav">
<a href="/sozoku/"><img src="/img_cmn/side/btn_sozoku.jpg" width="240" height="40" alt="btn_sozoku.jpg" class="btn" /></a>
<ul>
<li><a href="/sozoku/sozoku/">争族対策を考えている</a></li>
<li><a href="/sozoku/pay/">納税対策を考えている</a></li>
<li><a href="/sozoku/tax/">節税対策を考えている</a></li>
<li class="end"><a href="/sozoku/shokei/">事業承継を考えている</a></li>
</ul>
<a href="/fudosan/"><img src="/img_cmn/side/btn_fudosan.jpg" width="240" height="40" alt="btn_fudosan.jpg" class="btn" /></a>
<ul>
<li><a href="/fudosan/seiri/">借地・底地・借家を整理したい</a></li>
<li><a href="/fudosan/shobun/">貸宅地・古貸家・古アパートの整理・処分をしたい</a></li>
<li class="end"><a href="/fudosan/trouble/">境界トラブルを解決したい</a></li>
</ul>
<a href="/tax/"><img src="/img_cmn/side/btn_setsuzei.jpg" width="240" height="40" alt="btn_setsuzei.jpg" class="btn" /></a>
<ul>
<li><a href="/tax/kotei/">固定資産税を安くしたい</a></li>
<li><a href="/tax/shotoku/">所得税を安くしたい</a></li>
<li><a href="/tax/sozoku/">相続税の負担を少なくしたい</a></li>
<li><a href="/tax/zouyo/">贈与税を安くしたい</a></li>
<li class="end"><a href="/tax/kakutei/">確定申告で節税したい</a></li>
</ul>
<a href="/chintai/"><img src="/img_cmn/side/btn_chintai.jpg" width="240" height="40" alt="btn_chintai.jpg" class="btn" /></a>
<ul>
<li><a href="/chintai/free/">空室問題を解決したい</a></li>
<li><a href="/chintai/taino/">滞納問題を解決したい</a></li>
<li><a href="/chintai/tachinoki/">立退きを円滑に行いたい</a></li>
<li><a href="/chintai/build/">建物の修繕をしたい</a></li>
<li><a href="/chintai/taishin/">耐震診断／耐震補強をしたい</a></li>
<li><a href="/chintai/shikin/">資金繰りを改善したい</a></li>
<li class="end"><a href="/chintai/reform/">リフォーム・リノベーションをしたい</a></li>
</ul>
<a href="/tochi/"><img src="/img_cmn/side/btn_tochi.jpg" width="240" height="40" alt="btn_tochi.jpg" class="btn" /></a>
<ul>
<li><a href="/tochi/decision/">どうやって活用方法を決めるのか知りたい</a></li>
<li><a href="/tochi/know/">どんな活用方法があるのか知りたい</a></li>
<li class="end"><a href="/tochi/safe/">この事業（提案）で大丈夫か見てほしい</a></li>
</ul>
</div>
<!--end sidenav-->
<div class="sidebnr">
<ul>
<!--<li><a href="/works/"><img src="/img_cmn/side/bnr_jirei.jpg" width="240" height="45" alt="事例紹介" /></a></li>-->
<li><a href="/seminar/dvd/"><img src="/img_cmn/side/bnr_dvd.jpg" width="240" height="110" alt="DVD限定プレゼント" /></a></li>
<li><a href="/shisan/"><img src="/img_cmn/side/bnr_shisanunyo.jpg" width="240" height="49" alt="資産運用倶楽部" /></a></li>
</ul>
</div>
<div id="sidecontact">
<ul>
<li><a href="/mail/">メルマガの登録はこちら</a></li>
<li><a href="/mail/backnum/">バックナンバーはこちら</a></li>
</ul>
</div>
<!--end sidebnr-->
<div class="sidebnr">
<ul>
<li><a href="/specialist_blog/"><img src="/img_cmn/side/bnr_senmonka.jpg" width="240" height="70" alt="専門家コラム" /></a></li>
<li><a href="/blog/"><img src="/img_cmn/side/bnr_tsushin.jpg" width="240" height="61" alt="資産パートナープランナーズ通信" /></a></li>
<li><a href="https://twitter.com/#!/shisan_pp" target="_blank"><img src="/img_cmn/side/bnr_twitter.jpg" width="240" height="46" alt="相談できるツイッター" /></a></li>
</ul>
</div>
<!--end sidebnr-->
<div id="side_bottom">
<ul>
<li><a href="/privacy/">プライバシーポリシー</a></li>
<li><a href="/sitemap/">サイトマップ</a></li>
</ul>
</div>
<!--end side_bottom-->
</div>
<!--end side-->
<div id="main">