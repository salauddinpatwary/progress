<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="/css/layout_cmn.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php the_content(); ?>


<?php endwhile; else: ?>

<?php endif; ?>
</body>
</html>