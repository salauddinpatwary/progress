<?php 
get_header();?>
<article>
<div class="page_title_box">
<h1 class="page_title"><?php
if(is_category()){single_cat_title();}
elseif ( is_day() ){echo get_the_date();}
elseif ( is_month() ) {echo get_the_date('Y年F');}
elseif ( is_year() ){echo get_the_date('Y年');}
elseif(is_tag()){single_tag_title();}
elseif(is_post_type_archive()){post_type_archive_title();}
elseif(is_tax() ){single_term_title();}
elseif(is_author){the_author();echo 'の';}
else {echo ('新着');}?>記事一覧</h1>
</div>
<div class="container">
<div class="archive_post">
<?php if(have_posts()){?>
<ul>
<?php while(have_posts()){
the_post();?>
<li><dl><dt><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></dt><dd><?php the_excerpt();?></dd></dl></li>
<?php }?>
</ul>
<?php if(function_exists('wp_pagenavi')){
echo '<div class="pagelink">';
wp_pagenavi();
echo '</div>';}  wp_reset_query();


}else{
echo '<p class="nopost">ページがみつかりませんでした。</p>';
}?>
</div>
</div>
</article>
<?php get_footer();?>