<!DOCTYPE HTML>
<html <?php language_attributes(); ?> xmlns:fb="http://ogp.me/ns/fb#">
<head>
<meta charset="<?php wp_title(); ?>">
<title><?php bloginfo('name'); ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" media="all" />
<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url'); ?>/favicon.ico" />
<link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/screenshot.png" />
<?php
wp_deregister_script('jquery');
wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js', array(), '1.10.0');
wp_enqueue_script('jquery ui', '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', array(), '1.10.3');

wp_head(); ?>
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link rel="meta" href="<?php bloginfo('rdf_url'); ?>" />
</head>
<body <?php body_class(page_name()); ?>>
<header>
<div id="header">
<hgroup>
<h1><a href="<?php echo home_url();?>/"><img src="<?php bloginfo('template_url'); ?>/img/logo.png" width="398" height="70" alt="<?php the_title();?>" /></a></h1>
</hgroup>
</div>
</header>