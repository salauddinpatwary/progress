<?php 
/*
Template Name: オプトインページ「opt1」
*/
get_header();?>
<header>
<div id="header">
<hgroup>
<h1><img src="<?php bloginfo('template_url'); ?>/img/logo.png" width="398" height="70" alt="<?php the_title();?>" /></h1>
</hgroup>
</div>
</header>
<article>
<div class="container">
<section>
<div id="head_img" class="txc">
<img src="<?php bloginfo('template_url'); ?>/img/main_image.jpg" width="958" height="332" alt="「成功大家３％クラブ」とは

日本の大家業を活性化させたい！
全国の大家さん、または「これから大家さん」に、
ぜひトップの一握りの「成功大家」になって人生を
充実してもらいたいという想いから、

不動産経営のプロフェッショナルを集めて
講師とした、大家さんのための学びの場です。" /></div>
</section>
</div>
<div class="professional">
<div class="pro_title txc"><img src="<?php bloginfo('template_url'); ?>/img/professional_title.png" width="316" height="51" alt="プロフェッショナルパートナー" /></div>
<div class="photos"><img src="<?php bloginfo('template_url'); ?>/img/professional.png" width="792" height="174" alt="" /></div>
</div>
<div id="social" class="top_social"></div>
<div class="container">
<div id="gaiyou">
<div class="right gaiyou"><img src="<?php bloginfo('template_url'); ?>/img/gaiyou.png" width="247" height="247" alt="概要" /></div>
<div class="left gaiyou">
<p class="pbr">このクラブを通して勉強していただくと、<br />
6ヶ月間のカリキュラム終了時には不動産経営のプロフェッショナルとして<br />
卒業していただけるという、完全大家サポートプログラムとなっています。</p>
<p class="pbr">また、メンバー同士で楽しく学んでいただけるように、<br />
不動産経営以外の「カルチャー講座」や「サークル活動」なども準備中です。</p>
</div>
<div class="clear"></div>
</div>
<div class="profile" id="profile">
<div class="left">
<img src="<?php bloginfo('template_url'); ?>/img/nishida.png" alt="代表　西田芳明" /></div>
<div class="right">
<h3><img src="<?php bloginfo('template_url'); ?>/img/aisatsu.png" width="198" height="46" alt="代表挨拶" /></h3>
<p class="pbr">進和建設工業株式会社を母体とする進和グループの代表であり、<br />
「成功大家３％クラブ」代表の、西田芳明です。</p>

<p class="pbr">ご存知ない方もいらっしゃると思いますが、 大阪府の南部に位置する堺市というところで、昭和43年に私の父が会社を立ち上げ、 昭和62年に私は２代目社長に就任し、現在まで土地活用のコンサルティングで、 賃貸マンションなどの建築を行ってきました。</p>

<p class="pbr">社長になって間もない時、日本の建築コストが高いことに着目した私は、「ローコストマンション」の研究を始めました。</p>

<p class="pbr">「ローコスト」と聞くと、なんだか質が良くないように思う方も多いと思いますが、 私の場合は、高品質なものを低価格でご提供するために、元々、技術者（現場監督）だった特技を生かして、現場の作業性を見直して短い工期で終わるようにしたり、仕入れルートの開拓やＯＥＭなどにより、材料を安く仕入れるなどで、コストを２割下げることに成功しました。</p>

<p class="pbr">私が「ローコスト」にこだわったのは、そうすれば家賃を安くしても収支が合う、ということで、入居者も喜んで入居してくれ、満室になってオーナー様にも喜んでもらえると思ったからです。</p>

<p class="pbr">結果、それにより、多くのオーナー様に喜んでいただきましたし、評判が評判を呼んで、多くの賃貸マンションの建築を任せていただきました。 現在までに、約２５０棟以上の賃貸マンション・アパートの建築をさせて頂きました。</p>

<p class="pbr">その中で、たくさんの大家さんと出会い、今までは「入居の良いマンション」を建てることで、大家さんであるお客様にはご満足いただけると思っていたのですが、 実際に建築してから年数が経てば経つほどに、入居率の問題や、修繕、賃料の滞納他入居者に関するトラブル、借入返済など。さまざまな問題に遭遇し、困っているというお話を聞くようになり、 賃貸管理の専門の会社も立ち上げ、マンション経営のサポートを始めました。</p>
</div>
<div class="clear"></div>
</div>
<div id="kougi">
<div class="right"><img src="<?php bloginfo('template_url'); ?>/img/kouzanaiyou.jpg" width="247" height="247" alt="講義内容" /></div>
<div class="left">
<p class="pbr">あなたの悩みを解決する各部門の専門情報をメールでお届け。<br />
わかりやすい追加資料やレジュメなどを郵送でもお届け（有料）。</p>

<p class="pbr">有料会員には教科書と、入学後は毎月役立ち資料や、新情報を配信。<br />
コースによって365日電話・ＦＡＸ相談受付や、事業収支・確定申告の<br />
見直しや作成もサポート。</p>
<p class="pbr">また、生活を充実させる、カルチャー講座やサークル活動、<br />
毎月自動で溜るポイントで、旅行や商品の当たる抽選会にも参加可能</p>
<p class="pbr">学びと遊びを両方楽しめる、大家さんのためのクラブです。</p>
</div>
<div class="clear"></div>
</div>
<div id="curriculum">
<h3 class="txc"><img src="<?php bloginfo('template_url'); ?>/img/curriculum.jpg" width="301" height="41" alt="カリキュラム" /></h3>
<ol>
<li>Ⅰ　相続勉強法「相続で財産を減らしたくない！」</li>
<li>Ⅱ　節税対策「もう税金で苦しまない！」</li>
<li>Ⅲ　マンション経営を成功させる</li>
<li>Ⅳ　土地活用「ちょっと待って！失敗しない</li>
<li>Ⅴ　不動産投資「投資はギャンブルではない！正しく安全に投資する」</li>
</ol>
<p>などなど </p>
</div>
</div>
<p class="txc"><img src="<?php bloginfo('template_url'); ?>/img/form_title.png" width="709" height="74" alt="まずは無料会員へご入会下さい。" /></p>
<?php
if($_GET['ac']){
$af= esc_attr($_GET['ac']);
setcookie('ac',esc_attr($_GET['ac']),time()+60*60*24*14,'./');
}elseif($_COOKIE['ac']){
$af= esc_attr($_COOKIE['ac']);
}
$code ='<div id="top_form">
<form method="post" action="http://www.e-shinwa.net/ms/reg.cgi" target="_blank">
<input type="hidden" name="id" value="success" />
<input type="hidden" name="type" value="1" />
<input type="hidden" name="charset" value="utf8" />
<input type="hidden" name="mode" value="1" />

<dl><dt><font color="red">*</font><img src="'.get_bloginfo('template_url').'/img/mail.png" width="61" height="40" alt="メール" title="メールアドレス" /></dt><dd><input type="email" name="email" value="メールアドレス" size="30" onfocus="if (this.value == \'メールアドレス\') {this.value = \'\';}" /></dd>
<dt><font color="red">*</font> お名前</dt><dd><input type="text" name="name" size="30" /></dd></dl>
<div class="clear"></div>
<input type="hidden" name="free_1_0" value="'.$post->post_name.'">
<input type="hidden" name="free_2_0" value="'.$af.'">
<input type="submit" name="submit" id="opt-touroku" value="　登録　" onmousedown="gq("send", "event", "join", "メルマガ登録","'.$post->post_name.'");">

</form>
</div>';

echo  $code;
?>
<div class="head"></div>
<div id="members">
<div class="box">

<h3 class="txr"><img src="<?php bloginfo('template_url'); ?>/img/nakama.png" width="782" height="47" alt="全国の悩める大家さんが仲間です" /></h3>
これから大家さんになる人や、なって間もない大家さんの場合、 どの時期にどんなリスクがあるのか、どんな問題が起<p class="pbr">きやすいかなど、 未経験ですからわからず、漠然とした不安だけが心に残ると思います。</p>

<p class="pbr">しかし、実際に何年、何十年とマンション経営を続けている人だと、どの時期にこんなことが起きて、どんな対処をしてうまくいった・・・ という経験をしているので、その話を聞くと、参考になりますし、まだ訪れない未来の出来事に対して心の準備や対策を練ることもできるために、 ちょっと安心感も生まれます。</p>

<p class="pbr">「成功大家3％クラブ」とは、 これからマンション経営を始める方、すでにマンション経営をされている方に、『一握りの成功大家』になるためのノウハウや、どうすればうまくいくかという発想力を身につけていただくクラブです。</p>


</div>
</div>


</article>
<?php get_footer();?>