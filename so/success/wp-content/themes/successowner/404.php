<?php 
get_header();?>
<article>
<div class="page_title_box"><h1 class="page_title">404 Page Not Found...</h1></div>
<div class="container">
<div class="content_post">
<p style="text-align:center;font-size:2.5em;font-weight:bold;padding-top:120px;">お探しのページは<br />見つかりませんでした。</p>
<p style="text-align:center;padding-bottom:50px;font-size:12px;"><a href="<?php echo home_url();?>/">トップページ</a></p>
</div>
</div>
</article>
<?php get_footer();?>