<?php
//ウィジェット
register_sidebar();

/*

ショートコード集
■youtube
[scs-youtube id="youtubeID" 

■ソーシャルボタン
[scs-social]


■オプトフォーム
[scs-form]


*/
function social_shortcode(){
$code = '<div id="social"></div>';
return $code;
}
add_shortcode('scs-social', 'social_shortcode');

//macvideo
function macvideol_shortcode($id){
if(empty($id['mac'])){
$id['mac']='youtube_get';
}elseif($id['mac']=="imac"){
$id['mac']="smac";
}elseif($id['mac']=="note"){
$id['mac']="youtube_get";
}
$code = '</div></div><div class="'.$id['mac'].'">
<div class="back">
<iframe width="640" height="380" src="//www.youtube.com/embed/'.$id["id"].'?rel=0&hd=1&showinfo=0&fs=0&controls=1&autohide=1&modestbranding=0&iv_load_policy=3" frameborder="0" allowfullscreen></iframe>
</div>
</div><div class="box790s"><div class="content_post">';
return $code;
}
add_shortcode('qol-youtube', 'macvideol_shortcode');

//form vip
function form_shortcode($id){
global $post;
if($_GET['ac']){
$af= esc_attr($_GET['ac']);
setcookie('ac',esc_attr($_GET['ac']),time()+60*60*24*14,'./');
}elseif($_COOKIE['ac']){
$af= esc_attr($_COOKIE['ac']);
}
$code ='<div id="top_form">
<form method="post" action="http://www.e-shinwa.net/ms/reg.cgi" target="_blank">
<input type="hidden" name="id" value="success">
<input type="hidden" name="type" value="1">
<input type="hidden" name="charset" value="utf8">
<input type="hidden" name="mode" value="1">

<dl><dt><font color="red">*</font><img src="http://so.progress-pp.jp/success/wp-content/themes/successowner/img/mail.png" width="61" height="40" alt="メール" title="メールアドレス"></dt><dd><input type="email" name="email" value="メールアドレス" size="30" onfocus="if (this.value == \'メールアドレス\') {this.value = \'\';}"></dd>
<dt><font color="red">*</font> お名前</dt><dd><input type="text" name="name" size="30"></dd></dl>
<div class="clear"></div>
<input type="hidden" name="free_1_0" value="'.$post->post_name.'">
<input type="hidden" name="free_2_0" value="'.$af.'">
<input type="submit" name="submit" id="opt-touroku" value="　登録　" onmousedown="gq(" send",="" "event",="" "join",="" "メルマガ登録","'.$post->post_name.'");"="">
</form>
</div><div class="head"></div>';

return $code;
}
add_shortcode('g_form', 'form_shortcode');

//保護中という文字を削除
add_filter('protected_title_format', 'remove_protected');
function remove_protected($title) {
       return '%s';
}

//カスタムヘッダー
function custom_background(){
	add_theme_support( 'custom-background', array('default-color' => 'EBEBEB') );
}
add_action( 'after_setup_theme', 'custom_background' );

//-------サイト設定-------------------------------------------------------------------//
function qd_add_admin_menu() {
	add_theme_page('サイト設定', 'サイト設定', 7, __FILE__, 'qd_admin_page');
	wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'wp-color-picker-script', plugins_url('script.js', __FILE__ ), array( 'wp-color-picker' ), false, true );
}
add_action('admin_menu', 'qd_add_admin_menu');

function qd_admin_page(){
echo '<h1>サイト設定</h1>';
if($_POST['kiroku']){
echo '<div id="setting-error-settings_updated" class="updated settings-error"> 
<p><strong>設定を保存しました。</strong></p></div>';
}?>
<form id="qd_form" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">

<p><input type="submit" name="kiroku" value="変更を保存" class="button button-primary" /></p>
</form>
<?php
}
//-------投稿設定-------------------------------------------------------------------//

/*管理画面が開いたときに実行*/
add_action('admin_menu', 'add_qd_custom_box');
add_action('save_post', 'save_add_html_textarea');
 
/* カスタムフィールドを投稿画面に追加 */
function add_qd_custom_box() {
    add_meta_box('htmlx', 'html欄', 'add_html_textarea_qd', 'page', 'normal', 'low');
    add_meta_box('フォントサイズ', 'フォントサイズ', 'add_font_size_qd', 'page', 'normal', 'low');
    add_meta_box('htmlx', 'html欄', 'add_html_textarea_qd', 'post', 'normal', 'low');
    add_meta_box('htmlx', 'html欄', 'add_html_textarea_qd', 'product', 'normal', 'low');
    add_meta_box('htmlx', 'html欄', 'add_html_textarea_qd', 'presents', 'normal', 'low');
}
 
/* 投稿画面に表示するフォームのHTMLソース */
function add_html_textarea_qd() {
    $page_layout = get_post_meta( $_GET['post'], 'htmlx' );
   if ($page_layout[0]) { $page_html = $page_layout[0];}
echo '<p>ショートコード[htmlx]で任意の場所に下欄のhtmlxを挿入。スクリプトも可能！！</p><textarea name="htmlx" style="width:100%" rows="15">'.$page_layout[0].'</textarea>';}

function add_font_size_qd(){
    $font_size = get_post_meta( $_GET['post'], 'font_size' );
  	if($font_size){ $font_size = $font_size[0];}
echo '<p>コンテンツのフォントサイズ</p>
<p><input type="number" size="2" name="font_size" value="'.$font_size.'"></p>';
}

/* 設定したカスタムフィールドの値をDBに書き込む記述 */
function save_add_html_textarea( $post_id  ) {

$save_array_qd = array('htmlx','font_size');
for($s = 0 ; $s < count($save_array_qd);$s++){
	$mydata = null;
    $mydata = $_POST[$save_array_qd[$s]];
	if(isset($mydata)){
    if ( $mydata != get_post_meta( $post_id, $save_array_qd[$s] )) {
        update_post_meta( $post_id, $save_array_qd[$s], $mydata ) ;
    } else if ( "" == $mydata ) {
        delete_post_meta( $post_id, $save_array_qd[$s] ) ;
    }}
}
}

//ショートコード
function html_set() {
return post_custom('htmlx');
}
add_shortcode('htmlx', 'html_set');

//--------------------------------------------------------------------------//
function my_password_form($passform) {
$passform = preg_replace('/この投稿は/','このページは',$passform);
$passform = preg_replace('/表示する/','<br>表示する',$passform);
//$passform .= 'test';
return $passform;
}
add_filter( 'the_password_form', 'my_password_form' );


//facebook id 追加
function fb_id(){
}
add_action('wp_head', 'fb_id');

//iframeが消えないように
add_filter('tiny_mce_before_init', create_function( '$a','$a["extended_valid_elements"] = "iframe[id|class|title|style|align|frameborder|height|longdesc|marginheight|marginwidth|name|scrolling|src|width]"; return $a;') );

//Wp Generator非表示
remove_action('wp_head', 'wp_generator');

//投稿画面にcss適用
//add_editor_style('editerstyle.css');

//管理画面にcss適用
function wp_custom_admin_css() {
//echo "\n" . '<link rel="stylesheet" href="' .get_bloginfo('template_directory'). '/customadmin.css' . '" />' . "\n";
	echo '<style>@media only screen and (min-width: 800px){#wpbody-content #dashboard-widgets .postbox-container,#wpbody-content #dashboard-widgets #postbox-container-1 {width: 49.5%;}#wpbody-content #dashboard-widgets #postbox-container-3{
float:right;width:50.5%;}</style>';
}
add_action('admin_head', 'wp_custom_admin_css', 100);

//コメントフィールドの削除
remove_action('wp_head', 'feed_links_extra', 3);

//スラッグからIDゲット
function idget($slug) {
$pid = get_page_by_path($slug);$pid = $pid -> ID;
return $pid;
}

//抜粋を加工
function new_excerpt_more($more) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

//抜粋の長さ
function change_excerpt_mblength($length) {
    return 60;
}
add_filter('excerpt_mblength', 'change_excerpt_mblength');

//ページネーム 親のスラッグもゲット
function page_name(){
global $post;
if(is_page()){$page_name = get_page($post->id)->post_name;}else{$page_name = 'other';}
if( is_page() && $post->post_parent ){
$p = get_post($post->post_parent);
$page_name .= '-'.$p -> post_name;
}
return 'p_'.$page_name;
}
//body_class(page_name());

//カテゴリ選択時に入れ子に
function lig_wp_category_terms_checklist_no_top( $args, $post_id = null ) {
    $args['checked_ontop'] = false;
    return $args;
}
add_action( 'wp_terms_checklist_args', 'lig_wp_category_terms_checklist_no_top' );

//お問い合わせのメールアドレスチェック
add_filter( 'wpcf7_validate_email', 'wpcf7_text_validation_filter_extend', 11, 2 );
add_filter( 'wpcf7_validate_email*', 'wpcf7_text_validation_filter_extend', 11, 2 );
function wpcf7_text_validation_filter_extend( $result, $tag ) {
    $type = $tag['type'];
    $name = $tag['name'];
    $_POST[$name] = trim( strtr( (string) $_POST[$name], "\n", " " ) );
    if ( 'email' == $type || 'email*' == $type ) {
        if (preg_match('/(.*)_confirm$/', $name, $matches)){
            $target_name = $matches[1];
            if ($_POST[$name] != $_POST[$target_name]) {
                $result['valid'] = false;
                $result['reason'][$name] = '確認用のメールアドレスが一致していません';
            }
        }
    }
    return $result;
}
//追記→[email* your-email_confirm watermark"確認のため再度ご入力ください"]

//fancybox
function autoimglink_class ($content) {
	global $post;
	$pattern        = "/(<a(?![^>]*?rel=['\"]lightbox.*)[^>]*?href=['\"][^'\"]+?\.(?:bmp|gif|jpg|jpeg|png)['\"][^\>]*)>/i";
	$replacement    = '$1 class="fancyimg">';
	$content = preg_replace($pattern, $replacement, $content);
	return $content;
}
add_filter('the_content', 'autoimglink_class', 99);
//$(document).ready(function() {$("a.fancyimg").fancybox();});

//アイキャッチ画像
add_theme_support( 'post-thumbnails' );
//add_image_size('topworks',160, 100, true);

//不要な要目を消す
function remove_menu() {
    remove_menu_page('link-manager.php'); // リンク
}
add_action('admin_menu', 'remove_menu');
//ログイン画面ロゴを変更
function custom_login() {
echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('template_directory').'/login.css" />';
}
add_action('login_head', 'custom_login');

//セルフピンバック禁止
function no_self_ping( &$links ) {
$home = get_option( 'home' );
foreach ( $links as $l => $link )
if ( 0 === strpos( $link, $home ) )
unset($links[$l]);
}
add_action( 'pre_ping', 'no_self_ping' );


// サイトマップ
function sitemap_q( $entries = false, $entriesnum = false, $hatenabm = false ) 
{
$html = "";
$postsnumhtml = "";
$categories = get_categories();

foreach( $categories as $category )
{
if( empty( $category->category_parent ) )
{
if( $entriesnum == true )
{
$posts = get_posts( 'category=' .$category->cat_ID . '&posts_per_page=-1' );
$postsnumhtml = '&nbsp;('. count( $posts ) .')';
						}

$html .= '<li>';
$html .= '<a href="'. get_category_link( $category->cat_ID ) .'">'. $category->name .'</a>' . $postsnumhtml;

if( $entries == true ) $html .= list_postlist_categories_keni( $category->cat_ID, $hatenabm );

$html .= list_parent_categories_keni( $category->cat_ID, $entries, $entriesnum );
$html .= '</li>';
}
}

if( $html != "" ) $html = '<ul>'. $html .'</ul>';
echo( $html );
}

function list_parent_categories_keni( $parent_id = 0, $entries = false, $entriesnum = false )
{
$html = "";

$categories = get_categories( 'child_of=' . $parent_id );

foreach( $categories as $category )
{
if( $category->category_parent == $parent_id )
{
if( $entriesnum == true )
{
$posts = get_posts( 'category=' .$category->cat_ID . '&posts_per_page=-1' );
$postsnumhtml = '&nbsp;('. count( $posts ) .')';
}

$html .= '<li>';
$html .= '<a href="'. get_category_link( $category->cat_ID ) .'">'. $category->name .'</a>' . $postsnumhtml;
if( $entries == true ) $html .= list_postlist_categories_keni( $category->cat_ID, $hatenabm );
$html .= list_parent_categories_keni( $category->cat_ID, $entries, $entriesnum );

$html .= '</li>';
}
}

if( $html != "" ) return '<ul class="sub">'. $html .'</ul>';
else return $html;
}

function list_postlist_categories_keni( $category_id, $hatenabm = false )
{
global $post;

$html = "";

query_posts( 'cat=' .$category_id . '&posts_per_page=10' );

if( have_posts() )
{
while( have_posts() )
{
the_post();

if( in_category( $category_id ) )
{
$html .= '<li><a href="' . get_permalink( $post->ID ) . '">' . $post->post_title . '</a>';
if ( true == $hatenabm ) $html .= get_hatena_bookmark(get_permalink($post->ID));
$html .= '</li>';
}
}
}
wp_reset_query();

if( $html != "" ) $html = '<ul class="sub">' . $html . '</ul>';
return $html;
}

//カスタム投稿 コンテン
function add_contentsdirectory_type() {
$args = array(
'label' => 'コンテンツ',
'labels' => array(
'singular_name' => 'コンテンツ',
'add_new_item' => 'コンテンツ登録',
'add_new' => 'コンテンツ登録',
'new_item' => '新規コンテンツ',
'view_item' => 'コンテンツを表示',
'not_found' => 'コンテンツは見つかりませんでした。',
'not_found_in_trash' => 'ゴミ箱にはありません。',
'search_items' => 'コンテンツを検索',
),
'public' => true,
'menu_position' =>20,
'supports' => array('title', 'editor' , 'author' , 'thumbnail' , 'excerpt' , 'custom-fields','revisions','page-attributes'),
'publicly_queryable' => true,
'query_var' => true,
'rewrite' => true,
'hierarchical' => true,
'has_archive' => false,
);
register_post_type('contentsdirectory',$args);
flush_rewrite_rules();
}
add_action('init', 'add_contentsdirectory_type');

//カスタム投稿 プレゼント
function add_present_type() {
$args = array(
'label' => 'プレゼント',
'labels' => array(
'singular_name' => 'プレゼント',
'add_new_item' => 'プレゼント登録',
'add_new' => 'プレゼント登録',
'new_item' => '新規プレゼント',
'view_item' => 'プレゼントを表示',
'not_found' => 'プレゼントは見つかりませんでした。',
'not_found_in_trash' => 'ゴミ箱にはありません。',
'search_items' => 'プレゼントを検索',
),
'public' => true,
'menu_position' =>21,
'supports' => array('title', 'editor' , 'author' , 'thumbnail' , 'excerpt' , 'custom-fields','revisions','page-attributes'),
'capability_type' => 'page',
'publicly_queryable' => true,
'query_var' => true,
'rewrite' => true,
'hierarchical' => true,
'has_archive' => false,
);
register_post_type('present',$args);
flush_rewrite_rules();
}
add_action('init', 'add_present_type');

//onclick universal eventtracking
function onclick($content){
global $post;
$pattern = array(
'/<a(.*?)href="(.*?)"/u',
'/<input(.*?)type="submit"/u');

$replace = array(
'<a$1onmousedown="gq(\'send\', \'event\', \'link-click\', \'go-'.$post->post_title.'\',\'$2\');" href="$2"',
'<input$1type="submit" onmousedown="gq(\'send\', \'event\', \'contact-form\', \'push-'.$post->post_title.'\');" ');
if(is_page('opt3')||is_page('opt2')){}
else{
$content = preg_replace($pattern,$replace,$content);
}
return $content;

}
add_filter('the_content','onclick',99);


;?>