<?php 
/*
Template Name: オプトインページ「wi」
*/
get_header();?>
<article>
<div class="container">
<section>
<div id="head_img" class="txc"><?php the_content();?></div>
</section>
</div>
<?php /*<div class="professional">
<div class="pro_title txc"><img src="http://so.progress-pp.jp/success/wp-content/themes/successowner/img/professional_title.png" width="316" height="51" alt="プロフェッショナルパートナー"></div>
<div class="photos"><img src="http://so.progress-pp.jp/success/wp-content/themes/successowner/img/professional.png" width="792" height="174" alt=""></div>
</div>*/?>
<div id="social" class="top_social"></div>
<div class="pro_title txc"><img src="http://so.progress-pp.jp/success/wp-content/themes/successowner/img/professional_title.png" width="316" height="51" alt="プロフェッショナルパートナー"></div>
<div id="yasuhisa">
<div class="text">
<h3><img src="http://so.progress-pp.jp/success/wp-content/themes/successowner/img/hoyu-shikaku.png" width="81" height="22" alt="保有資格"/></h3>
<p>CPM（公認不動産経管理士）<br>
宅地建物取引主任者<br>
ＣＦＰ（上級ファイナンシャルプランナー）<br>
２級ファイナンシャルプランニング技能士<br>
不動産賃貸管理士<br>
ＮＰＯ法人相続アドバイザー協議会会員<br>
1級建築施工技術管理士<br>
日商簿記検定２級<br>
損害保険募集人<br>
日本防犯学校認定　セキュリティアドバイザー</p>
</div>
</div>
<div class="container">
<div id="gaiyou">
<div class="right gaiyou"><img src="http://so.progress-pp.jp/success/wp-content/themes/successowner/img/gaiyou.png" width="247" height="247" alt="概要"></div>
<div class="left gaiyou pbr"_box>
<?php the_field('gaiyou');?>
</div>
<div class="clear"></div>
</div>
<div class="profile" id="profile">
<div class="left">
<img src="http://so.progress-pp.jp/success/wp-content/themes/successowner/img/nishida.jpg" width="247" height="247" alt="代表　西田芳明"></div>
<div class="right pbr_box">
<?php the_field('profile');?>
</div>
<div class="clear"></div>
</div>
<div id="kougi">
<div class="right"><img src="http://so.progress-pp.jp/success/wp-content/themes/successowner/img/nyukaitokuten.png" width="247" height="247" alt="入会特典"></div>
<div class="left pbr_box">
<?php the_field('kougi');?>
</div>
<div class="clear"></div>
</div>
<div id="curriculum" class="pbr_box">
<?php the_field('curriculum');?>
</div>
</div>
<?php the_field('top_form');?>
<div id="members">
<div class="box pbr_box">
<?php the_field('members');?>
</div>
</div>
</article>
<?php get_footer();?>