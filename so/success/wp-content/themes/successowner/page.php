<?php 
get_header();?>
<article>
<div class="page_title_box"><h1 class="page_title"><?php the_post();the_title();?></h1></div>
<div class="container www">
<div class="content_post">
<?php the_content();?>
<div class="clear"></div>
</div>
</div>
</article>
<?php get_footer();?>