<?php 
get_header();?>
<article>
<header>
<div class="page_title_box"><h1 class="page_title"><?php the_post();the_title();?></h1></div>
</header>
<div class="container www">
<div class="content_post">
<?php the_content();?>
<div class="clear"></div>
</div>
<div id="page_link">
<span class="previous">
<?php previous_post_link( '%link', '%title &gt;'); ?>
</span>
<span class="next">
<?php next_post_link( '%link', '&lt; %title'); ?>
</span>
</div>
<?php the_category();?>
<div class="clear"></div>
</div>
</article>
<?php get_footer();?>